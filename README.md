[![Pipeline Status](https://gitlab.com/daddl3/liip_imagine_template_bundle/badges/v0.0.33/pipeline.svg)]() 
[![Version](https://img.shields.io/badge/version-v0.0.33-blue)](https://gitlab.com/daddl3/liip_imagine_template_bundle/blob/master/version.txt)

# Liip Imagine Template Symfony Bundle

Liip Integration for Symfony

## Getting started

This bundle helps you to use Liip Imagine Bundle 

## Installation

```bash
$ composer requ daddl3/liip_imagine_template_bundle
```

### Config

you have to config liip_imagine like this

`````yaml
liip_imagine:

  resolvers:
    data:
      web_path:
        web_root: "%kernel.project_dir%/public"
        cache_prefix: media/cache/resolve

  loaders:
    data:
      filesystem:
        data_root: "%kernel.project_dir%/data" // important
`````

### Avif and Webp
to enable one of them just add those
```yaml
    parameters:
        daddl3_liip_imagine_template.avif: true
        daddl3_liip_imagine_template.webp: true
```

##### Information

Some functions are just for my infrastructure