<?php

namespace daddl3\LiipImagineTemplateBundle\Twig\Extension;

use Override;
use daddl3\LiipImagineTemplateBundle\Twig\Runtime\ImageCreationExtensionRuntime;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class ImageCreationExtension extends AbstractExtension
{
    #[Override]
    public function getFunctions(): array
    {
        return [
            new TwigFunction('getImagineUrl', [ImageCreationExtensionRuntime::class, 'getImagineUrl']),
            new TwigFunction('getInfoWebp', [ImageCreationExtensionRuntime::class, 'getInfoWebp']),
            new TwigFunction('getWebPUrl', [ImageCreationExtensionRuntime::class, 'getWebPUrl']),
            new TwigFunction('generateWebP', [ImageCreationExtensionRuntime::class, 'generateWebP']),
            new TwigFunction('getMime', [ImageCreationExtensionRuntime::class, 'getMime']),
            new TwigFunction('getInfoAvif', [ImageCreationExtensionRuntime::class, 'getInfoAvif']),
            new TwigFunction('getAvifUrl', [ImageCreationExtensionRuntime::class, 'getAvifUrl']),
            new TwigFunction('getImageResolution', [ImageCreationExtensionRuntime::class, 'getImageResolution']),
            new TwigFunction('getCoverImage', [ImageCreationExtensionRuntime::class, 'getCoverImage']),
            new TwigFunction('checkVideo', [ImageCreationExtensionRuntime::class, 'checkVideo']),
            new TwigFunction('getRightFileUrl', [ImageCreationExtensionRuntime::class, 'getRightFileUrl']),
        ];
    }
}
