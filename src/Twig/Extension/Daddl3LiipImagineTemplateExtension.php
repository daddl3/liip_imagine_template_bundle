<?php

namespace daddl3\LiipImagineTemplateBundle\Twig\Extension;

use Override;
use daddl3\LiipImagineTemplateBundle\Twig\Runtime\Daddl3LiipImagineTemplateRuntime;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class Daddl3LiipImagineTemplateExtension extends AbstractExtension
{
    #[Override]
    public function getFunctions(): array
    {
        return [
            new TwigFunction('isWebpEnabled', [Daddl3LiipImagineTemplateRuntime::class, 'isWebpEnabled']),
            new TwigFunction('isAvifEnabled', [Daddl3LiipImagineTemplateRuntime::class, 'isAvifEnabled']),
        ];
    }
}
