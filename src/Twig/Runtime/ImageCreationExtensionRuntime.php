<?php

namespace daddl3\LiipImagineTemplateBundle\Twig\Runtime;

use Exception;
use http\Exception\RuntimeException;
use Liip\ImagineBundle\Exception\Binary\Loader\NotLoadableException;
use Liip\ImagineBundle\Imagine\Cache\CacheManager;
use Liip\ImagineBundle\Service\FilterService;
use Psr\Cache\InvalidArgumentException;
use Psr\Log\LoggerInterface;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Finder\Finder;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;
use Twig\Extension\RuntimeExtensionInterface;

class ImageCreationExtensionRuntime implements RuntimeExtensionInterface
{
    /**
     * Expire time = X seconds.
     */
    public const EXPIRE_TIME = 86400;

    public const IMAGE_QUALITY = 90;

    public const DEFAULT_WIDTH = 750;

    public const DEFAULT_HEIGHT = 500;

    private string $projectDir = '';

    public function __construct(
        private readonly LoggerInterface $logger,
        private readonly FilterService $filterService,
        private readonly UrlGeneratorInterface $urlGenerator,
        private readonly HttpClientInterface $httpClient,
        private readonly ParameterBagInterface $parameterBag
    ) {
        /** @var string $projectDir */
        $projectDir = $this->parameterBag->get('kernel.project_dir');
        $this->projectDir = $projectDir;
    }

    public function getMime(
        string $imagePath
    ): string {
        if (!realpath($imagePath)) {
            // to use a normal file and the aset function
            $file_path = $this->projectDir.'/data/'.$imagePath;
            $imagePath = $this->normalizePath($file_path);
        }

        return (new File($imagePath))->getMimeType();
    }

    /**
     * @return array<string,int>|null
     *
     * @throws InvalidArgumentException
     */
    public function getImageResolution(
        string $imageUrl = ''
    ): ?array {
        $imageResolution = [
            'width' => self::DEFAULT_WIDTH,
            'height' => self::DEFAULT_HEIGHT,
        ];

        if (empty($imageUrl)) {
            return $imageResolution;
        }

        $entityCachedName = $this->getImageEntityCachedName($imageUrl.'resolution', 'imageResolution');
        $filesystemAdapter = new FilesystemAdapter();

        $entityCached = $filesystemAdapter->getItem($entityCachedName);
        if (!$entityCached->isHit()) {
            $imageSrc = parse_url($imageUrl, \PHP_URL_PATH);
            $imageLocation = $_SERVER['DOCUMENT_ROOT'].$imageSrc;

            if (file_exists($imageLocation)) {
                $imageSize = getimagesize($imageLocation);

                if ($imageSize) {
                    $imageResolution['width'] = $imageSize[0];
                    $imageResolution['height'] = $imageSize[1];
                } else {
                    return $imageResolution;
                }
            } else {
                return $imageResolution;
            }

            $entityCached->set($imageResolution);
            $entityCached->expiresAfter(self::EXPIRE_TIME);
            $filesystemAdapter->save($entityCached);
        }

        return $entityCached->get();
    }

    /**
     * @return bool|array<string>|string
     */
    public function getCoverImage(
        string $folder
    ): bool|array|string {
        if ('placeholder' === $folder) {
            return 'news_pics/logo.jpg';
        }

        $file = new Finder();
        $teasers = $file->in($this->projectDir.'/data/news_pics/'.$folder);

        $teaserArray = [];

        foreach ($teasers as $teaser) {
            $file = new File($teaser->getRealPath());
            if ('video/mp4' === $file->getMimeType()) {
                if (is_file($this->projectDir.'/data/news_pics/'.$folder.'/thumbnail.png')) {
                    return 'news_pics/'.$folder.'/thumbnail.png';
                }

                break;
            }

            $mime = $file->getMimeType();
            if ('video/mp4' === $mime) {
                return 'news_pics/logo.jpg';
            }

            $teaserArray[] = $this->replaceServerDir($teaser->getRealPath());
        }

        shuffle($teaserArray);

        return array_shift($teaserArray);
    }

    public function checkVideo(
        string $videoPath,
        string $cacheFilter
    ): string {
        $cachePath = 'video/'.$cacheFilter;
        $cachedVideo = $cachePath.$videoPath;
        if (!file_exists($cachedVideo)) {
            $folders = explode('/', $videoPath);
            if (!file_exists($this->projectDir.'/public/video/') && !mkdir(
                $concurrentDirectory = $this->projectDir.'/public/video/'
            ) && !is_dir(
                $concurrentDirectory
            )) {
                throw new RuntimeException(sprintf('Directory "%s" was not created', $concurrentDirectory));
            }

            if (!file_exists($this->projectDir.'/public/video/'.$cacheFilter.'/') && !mkdir(
                $concurrentDirectory = \dirname(
                    __DIR__,
                    6
                ).'/public/video/'.$cacheFilter.'/'
            ) && !is_dir($concurrentDirectory)) {
                throw new RuntimeException(sprintf('Directory "%s" was not created', $concurrentDirectory));
            }

            if (!file_exists(
                $this->projectDir.'/public/video/'.$cacheFilter.'/'.$folders[1].'/'
            ) && !mkdir(
                $concurrentDirectory = \dirname(
                    __DIR__,
                    6
                ).'/public/video/'.$cacheFilter.'/'.$folders[1].'/'
            ) && !is_dir($concurrentDirectory)) {
                throw new RuntimeException(sprintf('Directory "%s" was not created', $concurrentDirectory));
            }

            if (!file_exists(
                \dirname(
                    __DIR__,
                    6
                ).'/public/video/'.$cacheFilter.'/'.$folders[1].'/'.$folders[2].'/'
            ) && !mkdir(
                $concurrentDirectory = \dirname(
                    __DIR__,
                    6
                ).'/public/video/'.$cacheFilter.'/'.$folders[1].'/'.$folders[2].'/'
            ) && !is_dir($concurrentDirectory)) {
                throw new RuntimeException(sprintf('Directory "%s" was not created', $concurrentDirectory));
            }

            copy(
                $this->projectDir.'/data'.$videoPath,
                \dirname(
                    __DIR__,
                    6
                ).'/public/video/'.$cacheFilter.'/'.$folders[1].'/'.$folders[2].'/'.$folders[3]
            );
        }

        return $cachedVideo;
    }

    /**
     * @throws InvalidArgumentException
     */
    public function getRightFileUrl(
        Request $request,
        string $imageUrl,
        string $imagineFilter = 'meta'
    ): string {
        if ('dev' !== $this->parameterBag->get('kernel.environment')) {
            if ($this->parameterBag->get('daddl3_liip_imagine_template.webp') && $this->getInfoAvif($request)) {
                return $this->getAvifUrl($imageUrl, $imagineFilter);
            }

            if ($this->parameterBag->get('daddl3_liip_imagine_template.avif') && $this->getInfoWebp($request)) {
                return $this->getWebPUrl($imageUrl, $imagineFilter);
            }
        }

        return $this->getImagineUrl($imageUrl, $imagineFilter);
    }

    public function getInfoAvif(
        Request $request
    ): string|bool {
        return (str_contains((string) $request->headers->get('accept'), 'image/avif')) ? 'image/avif' : false;
    }

    /**
     * @throws InvalidArgumentException
     */
    public function getAvifUrl(
        string $imageSrc = '',
        string $imagineFilter = ''
    ): string {
        return $this->getOrCreateCachedImage(
            imageSrc      : $imageSrc,
            imagineFilter : $imagineFilter,
            imageExtension: 'avif'
        );
    }

    public function generateImageUrl(
        string $imageUrl,
        string $imagineFilter = '',
        string $imageExtension = 'webp'
    ): string {
        try {
            /** @var string $imageUrl */
            $imageUrl = parse_url($imageUrl, \PHP_URL_PATH);
            $imageUrlPath = explode('/', $imageUrl);

            $imageName = end($imageUrlPath);

            $imagePathParts = array_diff($imageUrlPath, [$imageName]);
            $imagePath = implode('/', $imagePathParts);

            $imageUrl = str_contains($imageUrl, 'media') ? '/public'.$imageUrl : '/public/media'.$imageUrl;

            $file = new File($this->projectDir.$imageUrl);

            try {
                $img = match ($file->getMimeType()) {
                    'image/png' => imagecreatefrompng($file->getRealPath()),
                    default => imagecreatefromjpeg($file->getRealPath()),
                };
            } catch (Exception $e) {
                $this->logger->error($e->getMessage());
                $img = null;
            }

            $ImageNameParts = explode('.', $imageName);
            $ImageName = reset($ImageNameParts);

            $ImagePath = str_contains($imagePath, $imagineFilter) ? $imagePath : 'media/cache/'.$imagineFilter.$imagePath;

            $route = $this->urlGenerator->generate('home', [], UrlGeneratorInterface::ABSOLUTE_URL);

            $returnImageUrl = substr_replace(
                $route,
                '',
                -1
            ).$ImagePath.'/'.$ImageName.'.'.$imageExtension;

            $ImageFolder = '.'.$ImagePath.'/';

            if (!file_exists($ImageFolder) && (!mkdir($ImageFolder, 0777, true) && !is_dir($ImageFolder))) {
                throw new RuntimeException(sprintf('Directory "%s" was not created', $ImageFolder));
            }

            $ImageLocation = $ImageFolder.'/'.$ImageName.'.'.$imageExtension;
            try {
                if (!file_exists($ImageLocation) && (\is_object($img) && 'GdImage' === $img::class)) {
                    imagepalettetotruecolor($img);
                    imagealphablending($img, true);
                    imagesavealpha($img, true);

                    match ($imageExtension) {
                        'webp' => imagewebp($img, $ImageLocation, self::IMAGE_QUALITY),
                        'avif' => imageavif($img, $ImageLocation, self::IMAGE_QUALITY),
                        default => throw new Exception('no webp or avif')
                    };

                    imagedestroy($img);
                }
            } catch (Exception $e) {
                $this->logger->error($e->getMessage());
            }
        } catch (Exception $exception) {
            $this->logger->error('Webp exception: '.$exception);

            return '';
        }

        return $returnImageUrl;
    }

    /**
     * @throws InvalidArgumentException
     */
    public function getImagineUrl(
        string $imageSrc = '',
        string $imagineFilter = ''
    ): string {
        return $this->getOrCreateCachedImage(
            imageSrc     : $imageSrc,
            imagineFilter: $imagineFilter
        );
    }

    public function getInfoWebp(
        Request $request
    ): string|bool {
        return (str_contains((string) $request->headers->get('accept'), 'image/webp')) ? 'image/webp' : false;
    }

    /**
     * @throws InvalidArgumentException
     */
    public function getWebPUrl(
        string $imageSrc = '',
        string $imagineFilter = ''
    ): string {
        return $this->getOrCreateCachedImage(
            imageSrc      : $imageSrc,
            imagineFilter : $imagineFilter,
            imageExtension: 'webp'
        );
    }

    /**
     * @throws TransportExceptionInterface
     */
    public function getOneTeaser(
        string $folder,
        CacheManager $cacheManager
    ): mixed {
        if ('placeholder' === $folder) {
            return [
                'image' => 'news_pics/logo.jpg',
            ];
        }

        $file = new Finder();
        $teasers = $file->in($this->projectDir.'/data/news_pics/'.$folder);
        $teaserArray = [];
        $count = 0;
        $cachePath = $this->projectDir.'/public/media/cache/resolve/meta/news_pics/';

        if (!realpath($cachePath.$folder)) {
            foreach ($teasers as $teaser) {
                $file = new File($teaser->getRealPath());
                $mime = $file->getMimeType();
                if ('video/mp4' !== $mime) {
                    $this->generateFile(
                        $cacheManager->generateUrl('/news_pics/'.$folder.'/'.$teaser->getFilename(), 'meta')
                    );
                    $this->generateFile(
                        $cacheManager->generateUrl('/news_pics/'.$folder.'/'.$teaser->getFilename(), 'meta_tablet')
                    );
                    $this->generateFile(
                        $cacheManager->generateUrl('/news_pics/'.$folder.'/'.$teaser->getFilename(), 'meta_mobile')
                    );
                }
            }
        }

        foreach ($teasers as $teaser) {
            if ($count < 1) {
                ++$count;
                $file = new File($teaser->getRealPath());
                $mime = $file->getMimeType();
                if (('video/mp4' === $mime | str_contains((string) $file->getFilename(), 'thumbnail.png')) !== 0) {
                    return 'news_pics/'.$folder.'/thumbnail.png';
                }
            }

            $teaserArray[] = $this->replaceServerDir($teaser->getRealPath());
        }

        shuffle($teaserArray);

        return array_shift($teaserArray);
    }

    /**
     * @throws TransportExceptionInterface
     */
    public function generateFile(
        string $filePath
    ): ResponseInterface {
        return $this->httpClient->request(
            method: Request::METHOD_GET,
            url   : $filePath
        );
    }

    /**
     * @return array<string>|string
     *
     * @throws TransportExceptionInterface
     */
    public function getAll(
        string $folder,
        CacheManager $cacheManager
    ): array|string {
        if ('placeholder' === $folder) {
            return 'news_pics/logo.webp';
        }

        $file = new Finder();
        $images = $file->in($this->projectDir.'/data/news_pics/'.$folder);
        $cachePath = $this->projectDir.'/public/media/cache/resolve/slider_image/news_pics/';

        if (!realpath($cachePath.$folder)) {
            foreach ($images as $image) {
                $file = new File($image->getRealPath());
                $mime = $file->getMimeType();
                if ('video/mp4' !== $mime) {
                    $this->generateFile(
                        $cacheManager->generateUrl('/news_pics/'.$folder.'/'.$image->getFilename(), 'slider_image')
                    );
                    $this->generateFile(
                        $cacheManager->generateUrl(
                            '/news_pics/'.$folder.'/'.$image->getFilename(),
                            'slider_image_tablet'
                        )
                    );
                    $this->generateFile(
                        $cacheManager->generateUrl(
                            '/news_pics/'.$folder.'/'.$image->getFilename(),
                            'slider_image_mobile'
                        )
                    );
                }
            }
        }

        $image_array = [];
        foreach ($images as $image) {
            $image_array[] = 'news_pics/'.$folder.'/'.$image->getFilename();
        }

        return $image_array;
    }

    /**
     * @throws TransportExceptionInterface
     */
    public function checkHistory(
        Finder $finder,
        CacheManager $cacheManager
    ): void {
        $cachePath = $this->projectDir.'/public/media/cache/resolve/meta/history/';
        foreach ($finder as $picture) {
            if (!realpath($cachePath.$picture->getFilename())) {
                $this->generateFile($cacheManager->generateUrl('/history/'.$picture->getFilename(), 'meta'));
                $this->generateFile($cacheManager->generateUrl('/history/'.$picture->getFilename(), 'meta_tablet'));
                $this->generateFile($cacheManager->generateUrl('/history/'.$picture->getFilename(), 'meta_mobile'));
            }
        }
    }

    private function normalizePath(string $path): string
    {
        return preg_replace('#/+#', '/', $path);
    }

    private function getImageEntityCachedName(
        string $imageSrc = '',
        string $imagineFilter = ''
    ): string {
        /** @var string $cacheKey */
        $cacheKey = $this->parameterBag->get('cache_key') ?? '';

        $hash = md5($imageSrc.'-'.$imagineFilter);

        return sprintf('%sliip_imagine-%s', $cacheKey, $hash);
    }

    private function replaceServerDir(string $path): string
    {
        $hundePath = '/var/www/data/hunde';

        if (str_contains($path, 'hunde-master')) {
            $hundePath = '/var/www/hunde-master/data';
        }

        if (str_contains($path, 'hunde-staging')) {
            $hundePath = '/var/www/hunde-staging/data';
        }

        if (!realpath($hundePath)) {
            $hundePath = $this->projectDir.'/data';
        }

        return str_replace($hundePath, '', $path);
    }

    /**
     * @throws InvalidArgumentException
     */
    private function getOrCreateCachedImage(
        string $imageSrc = '',
        string $imagineFilter = '',
        string $imageExtension = 'jpg'
    ): string {
        if (empty($imageSrc) || empty($imagineFilter)) {
            return '';
        }

        $filesystemAdapter = new FilesystemAdapter();
        $entityCached = $filesystemAdapter->getItem(
            key: $this->getImageEntityCachedName($imageSrc.$imageExtension, $imagineFilter)
        );

        if (!$entityCached->isHit()) {
            $imageUrl = match ($imageExtension) {
                'webp' => $this->generateImageUrl(
                    imageUrl     : $this->getImagineUrl($imageSrc, $imagineFilter),
                    imagineFilter: $imagineFilter,
                ),
                'avif' => $this->generateImageUrl(
                    imageUrl      : $this->getImagineUrl($imageSrc, $imagineFilter),
                    imagineFilter : $imagineFilter,
                    imageExtension: 'avif'
                ),
                default => $this->generateJpgImage(
                    imageSrc     : $imageSrc,
                    imagineFilter: $imagineFilter
                ),
            };

            $entityCached->set(value: $imageUrl);
            $entityCached->expiresAfter(time: self::EXPIRE_TIME);
            $filesystemAdapter->save($entityCached);
        }

        return $entityCached->get();
    }

    private function generateJpgImage(
        string $imageSrc,
        string $imagineFilter
    ): string {
        try {
            $resolvedImageUrl = $this->filterService->getUrlOfFilteredImage(
                path  : $imageSrc,
                filter: $imagineFilter
            );
        } catch (NotLoadableException $notLoadableException) {
            $this->logger->error('Imagine exception: '.$notLoadableException);

            return '';
        }

        $imagineUrl = $resolvedImageUrl;
        $relativeImagineUrlArray = explode('/', $imagineUrl);
        array_splice($relativeImagineUrlArray, 0, 3);

        return '/'.implode('/', $relativeImagineUrlArray);
    }
}
