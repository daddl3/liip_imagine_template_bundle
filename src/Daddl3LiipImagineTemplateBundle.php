<?php

namespace daddl3\LiipImagineTemplateBundle;

use Override;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;
use Symfony\Component\HttpKernel\Bundle\AbstractBundle;

class Daddl3LiipImagineTemplateBundle extends AbstractBundle
{
    /**
     * @param array<string> $config
     */
    #[Override]
    public function loadExtension(array $config, ContainerConfigurator $containerConfigurator, ContainerBuilder $containerBuilder): void
    {
        $containerConfigurator->import('../config/services.yaml');

        $containerConfigurator->parameters()
            ->set('daddl3_liip_imagine_template.webp', false)
            ->set('daddl3_liip_imagine_template.avif', false);
    }
}
