<?php

namespace daddl3\LiipImagineTemplateBundle\Twig\Runtime;

use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Twig\Extension\RuntimeExtensionInterface;

class Daddl3LiipImagineTemplateRuntime implements RuntimeExtensionInterface
{
    public function __construct(private readonly ParameterBagInterface $parameterBag)
    {
    }

    public function isWebpEnabled(): bool
    {
        /** @var bool $enabled */
        $enabled = $this->parameterBag->get('daddl3_liip_imagine_template.webp');

        return $enabled;
    }

    public function isAvifEnabled(): bool
    {
        /** @var bool $enabled */
        $enabled = $this->parameterBag->get('daddl3_liip_imagine_template.avif');

        return $enabled;
    }
}
